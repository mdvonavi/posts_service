import requests
import os
from json import dumps
import hashlib

test_image_file_uploaded = 'test_upload.jpg'
test_image_file = 'test.jpg'

base_post_url = f'{os.getenv("REGRESS_TEST_HOST_POSTS")}'
base_user_url = f'{os.getenv("REGRESS_TEST_HOST_USERS")}'

post_url = base_post_url + "/posts"
user_url = base_user_url + "/users"

post_data = {
    "title": "test title", 
    "userId": 1, 
    "descriptions": "test descriptions"
}

user_data = {
    "sex": True,
    "city": 1,
    "avatar": "dummyLink",
    "info": "some info string",
    "firstName": "Ivan",
    "lastName": "Ivanov",
    "middleName": "Ivanovich",
    "nickname": "ivanoff",
    "email": "ivanoff@mail.ru",
    "phone": "79991234567",
    "birthDate": "2200-08-15"
}


def create_user(user_data: dict)-> int: 
    """POST запрос для создания нового пользователя"""
    response = requests.post(user_url, json=user_data)

    if response.status_code == 200:
        new_user_id = response.text.split()[-1].strip()
        print(f"Пользователь успешно создан с ID: {new_user_id}")
        return new_user_id
    else:
        print(f"Ошибка при создании пользователя. Код ошибки: {response.status_code}")
        return -1


def create_post_no_images(post_data: dict)-> int:
    """POST запрос для создания нового поста без изображений"""
    multipart_form_data = {
    'post': (None, dumps(post_data))
}

    response = requests.post(f"{post_url}", files=multipart_form_data)

    if response.status_code == 200:
        new_post_id = response.text.split()[-1].strip()
        print(f"Пост успешно создан с ID: {new_post_id}")
        return new_post_id

    else:
        print(f"Ошибка при создании поста. Код ошибки: {response.status_code}")
        return -1


def create_post_with_images(post_data: dict, image: str)-> int: 
    """POST запрос для создания нового поста с изображениями"""
    multipart_form_data = {
    'post': (None, dumps(post_data)),
    'imageFiles': (image, open(image, 'rb'))
}
    
    response = requests.post(f"{post_url}", files=multipart_form_data)

    if response.status_code == 200:
        new_post_id = response.text.split()[-1].strip()
        print(f"Пост успешно создан с ID: {new_post_id}")
        return new_post_id

    else:
        print(f"Ошибка при создании поста. Код ошибки: {response.status_code}")
        return -1


def get_post(id: int)-> dict:
    """GET запрос для получения поста"""
    response = requests.get(f"{post_url}/{id}")

    if response.status_code == 200:
        post = response.json()
        return post
    else:
        print(f"Ошибка при получении поста. Код ошибки: {response.status_code}")
        return {}


def get_user_posts(user_id: int)-> list:
    """GET запрос для получения постов пользователя"""
    response = requests.get(f"{post_url}/user/{user_id}")

    if response.status_code == 200:
        post = response.json()
        return post
    else:
        print(f"Ошибка при получении постов. Код ошибки: {response.status_code}")
        return []


def delete_post(post_id: int)-> int:
    """DELETE запрос для удаления поста пользователя"""
    response = requests.delete(f"{post_url}/{post_id}")

    if response.status_code == 200:
        return 0
    else:
        print(f"Ошибка при удаления поста. Код ошибки: {response.status_code}")
        return -1

def get_post_image(post_id: int, image_id: int)-> None:
    """GET запрос для получения изображения"""
    response = requests.get(f"{post_url}/{post_id}/image/{image_id}", allow_redirects=True)
    if response.status_code == 200:
        open(test_image_file_uploaded, 'wb').write(response.content)
    else:
        print(f"Ошибка при получении изображения. Код ошибки: {response.status_code}")

def md5(fname)-> str:
    """Рассчет хэшсуммы файла"""
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()

print("старт regress тестов для сервиса постов")
print("healthcheck")
response_post = requests.get(base_user_url)
assert response_post.status_code == 200
print("user service OK")

response_post = requests.get(base_post_url)
assert response_post.status_code == 200
print("post service OK")

print("создадим тествого пользователя")
user_id = create_user(user_data)
assert user_id != -1
print(f"пользователь с id={user_id} успешно создан")

print("создадим пост без изображений")
post_data['userId'] = user_id

post_id_no_images = create_post_no_images(post_data)
assert post_id_no_images != -1
print(f"пост с id={post_id_no_images} успешно создан")

print(f"получим пост с id={post_id_no_images}")
post = get_post(post_id_no_images)
assert int(post['id']) == int(post_id_no_images)
assert int(post['userId']) == int(user_id)
assert post['title'] == post_data['title']
assert post['descriptions'] == post_data['descriptions'] 
assert post.get('images') == []
print("данные в посте соответсвуют ожидаемым")

print(f"создадим пост с изображением")
post_id_w_images = create_post_with_images(post_data, test_image_file)
assert post_id_w_images != -1
print(f"пост с id={post_id_w_images} успешно создан")

print(f"получим пост с id={post_id_w_images}")
post = get_post(post_id_w_images)
assert int(post['id']) == int(post_id_w_images)
assert int(post['userId']) == int(user_id)
assert post['title'] == post_data['title']
assert post['descriptions'] == post_data['descriptions'] 
assert post.get('images') != []
get_post_image(post['id'], post['images'][0])
assert md5(test_image_file_uploaded) == md5(test_image_file)
print("данные в посте соответсвуют ожидаемым")

print(f"получим список постов пользователя с id={user_id}")
user_posts = get_user_posts(user_id)
assert len(user_posts) == 2
print("количество постов соответсвует ожидаемым")

print(f"удалим пост с id={post_id_no_images}")
delete_post(post_id_no_images)
assert get_post(post_id_no_images) == {}
print("при запросе пост ожидаемо не найден")

print(f"удалим пост с id={post_id_w_images}")
delete_post(post_id_w_images)
assert get_post(post_id_w_images) == {}
print("при запросе пост ожидаемо не найден")

print(f"получим список постов пользователя с id={user_id}")
user_posts = get_user_posts(user_id)
assert len(user_posts) == 0
print("количество постов соответсвует ожидаемым")

print("тест успешно завершён")