package ru.skillbox.demo;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpMethod;

import java.util.Objects;

public class UserHandler {
    private final String userServiceUrl;

    public UserHandler(String userServiceUrl) {
        this.userServiceUrl = userServiceUrl;
    }


    public boolean existsById(Long userId) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>("", headers);

        System.out.println(userServiceUrl);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                userServiceUrl + "/users/" + userId,
                HttpMethod.GET,
                requestEntity,
                String.class
        );

        return responseEntity.getStatusCode().is2xxSuccessful();
    }

    public Long createUser(String user) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> requestEntity = new HttpEntity<>(user, headers);

        System.out.println(userServiceUrl);

        ResponseEntity<String> responseEntity = restTemplate.exchange(
                userServiceUrl + "/users",
                HttpMethod.POST,
                requestEntity,
                String.class
        );

        return Long.parseLong(Objects.requireNonNull(responseEntity.getBody()).split("=")[1].strip());
    }
}
