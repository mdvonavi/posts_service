package ru.skillbox.demo.repository;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;

@Component
public class S3PostRepository {
    @Value("${aws.s3.bucket}")
    private String bucket;

    public S3PostRepository(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    private final AmazonS3 s3Client;

    public Collection<S3ObjectSummary> listObjects(String prefix) {
        return s3Client.listObjectsV2(bucket, prefix)
                .getObjectSummaries();
    }

    public void delete(String key) {
        s3Client.deleteObject(bucket, key);
    }

    public void put(String key, InputStream inputStream, ObjectMetadata metadata) {
        s3Client.putObject(bucket, key, inputStream, metadata);
    }

    public Optional<S3Object> get(String key) {
        try {
            return Optional.of(s3Client.getObject(bucket, key));
        } catch (AmazonServiceException exception) {
            return Optional.empty();
        }
    }
}
