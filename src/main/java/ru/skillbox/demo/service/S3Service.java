package ru.skillbox.demo.service;


import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import ru.skillbox.demo.repository.S3PostRepository;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Service

public class S3Service {
    private final S3PostRepository s3PostRepository;

    public S3Service(S3PostRepository s3PostRepository) {
        this.s3PostRepository = s3PostRepository;
    }

    public InputStream getImageContent(String key) {
        return s3PostRepository.get(key)
                .map(S3Object::getObjectContent)
                .orElseThrow(() -> new IllegalStateException("Object not found " + key));
    }

    public void putImage(String key, File file) throws IOException {
        try (var stream = new FileInputStream(file)) {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(file.length());
            metadata.setContentType(MediaType.TEXT_HTML_VALUE);
            s3PostRepository.put(key, stream, metadata);
        }
    }

    public void deleteImage(String key) {
        s3PostRepository.delete(key);
    }
}