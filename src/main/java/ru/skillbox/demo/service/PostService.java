package ru.skillbox.demo.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import io.minio.errors.MinioException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import ru.skillbox.demo.PostConvertor;
import ru.skillbox.demo.UserHandler;
import ru.skillbox.demo.dto.PostDto;
import ru.skillbox.demo.entity.Post;
import ru.skillbox.demo.entity.PostImage;
import ru.skillbox.demo.repository.PostImageRepository;
import ru.skillbox.demo.repository.PostRepository;
import ru.skillbox.demo.repository.S3PostRepository;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PostService {
    private static final Logger log = LoggerFactory.getLogger("PostService");

    @Autowired
    private final PostRepository postRepository;

    @Autowired
    private final PostImageRepository postImageRepository;

    @Autowired
    private final S3PostRepository s3PostRepository;

    @Autowired
    private final UserHandler userHandler;

    @Autowired
    private final PostConvertor postConvertor;


    public PostService(
            PostRepository postRepository,
            PostImageRepository postImageRepository,
            S3PostRepository s3PostRepository,
            UserHandler userHandler, PostConvertor postConvertor) {
        this.postRepository = postRepository;
        this.postImageRepository = postImageRepository;
        this.s3PostRepository = s3PostRepository;
        this.userHandler = userHandler;
        this.postConvertor = postConvertor;
    }

    public String createPost(PostDto postDto) {
        Post savedPost = postRepository.save(postConvertor.dtoToPost(postDto));
        return String.format("post added with id = %s",  savedPost.getId());
    }

    public PostDto getPost(long id) {
        Post post = postRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        post.setImages(getImageListByPostId(id));
        return postConvertor.postToDto(post);
    }

    private List<String> getImageListByPostId(long id) {
        List<PostImage> imageList = postImageRepository.findByPostId(id);
        List<String> stringList = new ArrayList<>();

        for (PostImage image : imageList) {
            stringList.add(image.getId().toString());
        }
        return stringList;
    }

    public String deletePost(long id) {
        if (!postRepository.existsById(id)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        postRepository.deleteById(id);
        return String.format("Post deleted with id = %s", id);
    }

    public List<Post> getAllPosts() {
        return postRepository.findAll();
    }

    public List<PostDto> getUserPosts(long userId) {
        List<Post> userPosts = postRepository.findByUserId(userId);
        for (Post post : userPosts) {
            post.setImages(getImageListByPostId(post.getId()));
        }
        return userPosts.stream().map(postConvertor::postToDto).collect(Collectors.toList());
    }

    private List<String> saveImagesToMinio(List<MultipartFile> imageFiles)
            throws IOException,
            MinioException,
            NoSuchAlgorithmException,
            InvalidKeyException {

        List<String> imageIds = new ArrayList<>();

        for (MultipartFile imageFile : imageFiles) {
            String imageId = UUID.randomUUID() + "-" + imageFile.getOriginalFilename();

            try (InputStream stream = imageFile.getInputStream()) {
                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentLength(imageFile.getSize());
                metadata.setContentType(MediaType.IMAGE_JPEG_VALUE);
                log.info("S3 image key = " + imageId);
                s3PostRepository.put(imageId, stream, metadata);
            }

            imageIds.add(imageId);
        }

        return imageIds;
    }

    public String savePost(Post post, List<MultipartFile> imageFiles) {
        if (post.getUserId() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        if (!userHandler.existsById(post.getUserId())) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        try {
            Post savedPost = postRepository.save(post);

            if (imageFiles != null) {
                List<String> imageIds = saveImagesToMinio(imageFiles);
                createPostImageEntities(savedPost, imageIds);
            }

            return "post saved with id = " + savedPost.getId().toString();
        } catch (IOException | InvalidKeyException | NoSuchAlgorithmException | MinioException e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<PostImage> createPostImageEntities(Post post, List<String> imageIds) {
        List<PostImage> postImages = new ArrayList<>();

        for (String imageId : imageIds) {
            PostImage postImage = new PostImage();
            postImage.setPostId(post.getId());
            postImage.setEtag(imageId);

            postImages.add(postImageRepository.save(postImage));
        }

        return postImages;
    }

    public String updatePost(String postString, List<MultipartFile> imageFiles) {
        return "updated";
    }


    public ResponseEntity<Object> getPostImage(long postId, long imageId, HttpServletResponse response) throws IOException {
        Optional<PostImage> image = postImageRepository.findById(imageId);
        if (image.isPresent()) {
            String imageLink =  image.get().getEtag().toString();

            InputStream stream = s3PostRepository.get(imageLink)
                    .map(S3Object::getObjectContent)
                    .orElseThrow(() -> new IllegalStateException("Object not found " + imageLink));

            response.setContentType("application/jpeg");
            response.setHeader("Content-Disposition", "inline; filename=\"" + imageLink + "\"");
            stream.transferTo(response.getOutputStream());

            return ResponseEntity.ok().build();

        } else {
            throw new IllegalStateException("Object not found. imageId = " + imageId);
        }


    }
}
