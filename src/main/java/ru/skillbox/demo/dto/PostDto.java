package ru.skillbox.demo.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostDto {
    private static final Logger log = LoggerFactory.getLogger("PostDto");

    private Long id;
    private String title;
    private Long userId;
    private String descriptions;
    private Date timestamp;
    private List<String> images;

    public PostDto() {
    }

    public PostDto(Long id, String title, Long userId, String descriptions, Date timestamp, List<String> images) {
        this.id = id;
        this.title = title;
        this.userId = userId;
        this.descriptions = descriptions;
        this.timestamp = timestamp;
        this.images = images;
    }

    public PostDto(String title, Long userId, String descriptions) {
        this.title = title;
        this.userId = userId;
        this.descriptions = descriptions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String toString() {
        Map<String, Object> post = new HashMap<>();

        if (this.id != null) {
            post.put("id", this.id);
        }
        if (this.title != null) {
            post.put("title", this.title);
        }
        if (this.descriptions != null) {
            post.put("descriptions", this.descriptions);
        }
        if (this.userId != null) {
            post.put("userId", this.userId);
        }
        if (this.images != null) {
            post.put("images", this.images);
        }

        ObjectMapper objectMapper = new ObjectMapper();

        String out = "{}";

        try {
            out = objectMapper.writeValueAsString(post);

        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return out;
    }

}
