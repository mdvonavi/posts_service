import requests
import os

base_url = f'{os.getenv("SMOKE_TEST_HOST")}'

print(base_url)

response_get = requests.get(base_url)

assert response_get.status_code == 200
